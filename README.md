Source code for Pholar project website

- https://pholar.gitlab.io/
- https://gitlab.com/pholar/pholar

Build with

    cobalt build

Develop

    (term 1) cobalt serve --no-watch
    (term 2) while true; do inotifywait -e close_write -r @./public/ @./.git/ .; cobalt clean; cobalt build; done

