---
title: Install 
published_date: 2021-01-01 23:00:00 +0000
---

# Install Pholar

Pholar is developed and tested on Linux.

## Requirements

- a webserver with php support
- perl >= 5.28
- sqlite >= 3.27
- imagemagick >= 6.9
- php >= 7.3
- php-gd
- php-imagick
- php-pdo
- php-pdo-sqlite
- php-xml
- php-curl
- php-mbstring
- php-zip
- php-json
- `file` utility


## Optional

- php-inotify (for `fs:watch` console command)
- ffmpeg >= 4.1 (for video/audio support)
- libreoffice (for documents support)
- ghostscript (for pdf/postscript and documents thumbs)

**note:** versions are from current package versions in Debian Buster (currently 'stable')
Pholar is tested on Debian Stable and Archlinux.


## Install

Details on how to install a webserver to serve Pholar are not discussed
in this guide.
The main point is that the webserver must serve Pholar from the root of
a domain (or subdomain) starting from the `public` folder in the repository.

Pholar can be installed cloning the git repository or downloading the 
repository as a compressed archive.
At this time there is still no "stable" release, and development is 
ongoing in `master` git branch.


### From git

``` sh
git clone https://gitlab.com/pholar/pholar.git
cd pholar 
```


### From tarball

```sh
wget https://gitlab.com/pholar/pholar/-/archive/master/pholar-master.tar.gz
tar xzvf pholar-master.tar.gz
mv pholar-master pholar
cd pholar
```


### Install PHP dependencies

```sh
bin/getcomposer
bin/composer.phar install --no-dev
```

### Configure

Main configuration for Phar is a file named `.env`
We provide a skeleton file to be used as base:

```sh
cp .env.dist .env
```

The skeleton file has comment describing every options.
Optional settings are commented out and presented with default values.

The main setting to change is `STORAGEDIR`, which must be set to the folder
with the media files to indicize.

Remember to set `DEBUG` to `false` in production once everything works.

Refer to the [Administration Guide](./administration/) for details on 
available options.

### Create database and default users

```sh
bin/console migrate
```
will create the database file with the latest schema

```sh
bin/console setupusers
```

will create the administrator and the guest user. By default the administrator
username is `admin`, but can be changed passing the desidered username as 
argument to the command.

Refer to the command help with

```sh
bin/console setupusers --help
```

### Set permissions

Folders `var/` and `public/thumb` must be writable by the user which run the 
php scripts. Usually that's the user the webserver run as.

``` sh
chown -R www-data:www-data var public/thumb
```

Change user and group as required per your setup.

### Install `php-inotify` via PECL

`php-inotify` is required by the console command `fs:watch`, which watch the 
storage folder for changes and update the database automatically.
If your distro doesn't provide `php-inotify` in its repository, you must 
install it using PECL.

Here a quick howto to intall `php-inotify` on Debian:

```sh
apt install php-pear php-dev
pecl install inotify
echo "extension=inotify.so" > /etc/php/7.3/mods-available/inotify.ini
phpenmod -s cli inotify
```

## What's next

Refer to the [Administration Guide](./administration/) to learn how to
configure your Pholar installation, keep the database updated and how to 
manage users.
