---
title: Documentation
published_date: 2021-01-01 23:59:00 +0000
---

# Pholar

Pholar allows to search, edit, group and share media files from a folder on disk in a simple web interface.

## Features

- store metadata in database for fast search
- simple query language
- show thumbnail for a reasonable range of media files (images, videos, documents)
- edit metadata and tags
- create collections of media for quick access
- multiuser with basic permission system
- allow public access to media or keep everything private
- share media via public links, with optional passwords, expiration date and custom permissions
- writeback as much as possible of edited medatata

## Documentation

- [Install](./install.html)
- [Administration Guide](./administration/)
- [User Guide](./user/)

