---
title: User guide 
published_date: 2021-01-01 20:00:00 +0000
categories: ["User guide"]
---

# User Guide

## The photo list page

### Sort

### Batch actions

#### Download selected

#### Update metadata

#### Add tags

#### Add to collection

#### Delete photos

## Search

### Tag search

## Detail page

### Edit metadata

## Collection

### Create collection

### Remove photos from collection

### Edit and delete collection

### Share collection

